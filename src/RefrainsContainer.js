import React, {useEffect, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Button from "@material-ui/core/Button";
import RefrainsData from "./RefrainsData";
import RefrainComponent from "./RefrainComponent";
import TextField from '@material-ui/core/TextField';
import Typography from "@material-ui/core/Typography";
import Chip from "@material-ui/core/Chip";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        padding: 1.5,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: '#ff9f82',
    },
    boxes: {
        justify: 'space-around',
        direction: 'row',
        alignItems: 'center',
        spacing: 3,
    },

    chip: {}
}));


// useState is a function that gets a value and an array
// refrains[randomNum] is the initial value that we receive back in the variable [randomRefrain]

export default function RefrainContainer() {
    const classes = useStyles();
    const handleChange = event => setGuess(event.target.value);
    const handleChangeName = event => setPlayerName(event.target.value);

const refrains = RefrainsData.map(lyrics => <RefrainComponent key={lyrics.id} data={lyrics} genre={lyrics.genre}/>);


    const randomNum = Math.floor(Math.random() * refrains.length);

    const gameStartMsg = "Type In Your Name and Hit Enter!";

    const [points, setPoints] = useState(0);
    const [randomRefrain, setRandomRefrain] = useState(gameStartMsg);
    const [guess, setGuess] = useState("");
    const [hasStarted, setHasStarted] = useState(false);
    const [hasGuessed, setHasGuessed] = useState(false);
    const [hasLost, setHasLost] = useState(false);
    const [playerName, setPlayerName] = useState("");
    const [enterName, hasEnteredName] = useState(false);

    const placeholderMsg = "Press Enter or hit GUESS to guess!";
    const placeholderMsgName = hasStarted ? playerName : "Enter Your Name!";

    const [timeLeft, setTimeLeft] = useState("10");
    const gameOverMsg = points > 0 ?
        points > 1 ?
            "Good Job " + playerName + "! You've scored " + points + " Points!" :
            "Good Job " + playerName + "! You've scored " + points + " Point!"
        :
        "Uuuuh, " + playerName + "! Give it another shot, " + points + " Points!";

    function Timer() {
        useEffect(() => {
                const interval = setInterval(() => {
                    setTimeLeft(prevTimeLeft => prevTimeLeft - 1);
                    console.log(points)
                }, 1000);
                return () => {
                    clearInterval(interval)
                };
            }, []
        );
        if (timeLeft <= 0) {
            gameOver();
        }
        return <p>{timeLeft}</p>
    }

    function displayRandomRefrain() {
        setRandomRefrain(
            (refrains[randomNum])
        );

    }

    function randomizePlus() {
        displayRandomRefrain();
        setPoints(prevPoints => prevPoints + 1);
    }

    function randomizeMinus() {
        if (!hasGuessed) {
            displayRandomRefrain();
            setTimeLeft(prevState => 10)
        } else {
            setPoints(prevPoints => prevPoints - 1);
            if (points === 0) {
                gameOver()
            } else {
                displayRandomRefrain();
            }
        }
    }

    function checkGuess() {
        setHasGuessed(prevState => true);
        if (randomRefrain.props.data.song === guess.toLowerCase().trim()) {
            randomizePlus();
            setGuess(prevGuess => "")
        } else {
            points === 0 ?
                gameOver() :
                randomizeMinus();
            setGuess(prevGuess => "");
        }
    }


    function gameOver() {
        setHasStarted(prevState => false);
        setHasGuessed(prevState => false);
        setHasLost(prevState => true);
    }

    function enableName() {
        setHasLost(prevState => false);
    }

    function gameOn() {
        setHasStarted(prevState => true);
        setPoints(0);
        displayRandomRefrain();
        setGuess(prevGuess => "");
        setTimeLeft(prevState => 10)
    }


    function keyPress(e) {
        if (e.keyCode === 13) {
            if (hasStarted) {
                if (guess.trim() !== '') {
                    checkGuess()
                }
            } else {
                if (playerName.trim() !== '') {
                    hasEnteredName(prevState => !prevState);
                    gameOn()
                }
            }
        }
    }

    return (

        <div className={classes.root}>

            <Grid className={classes.boxes} container spacing={3}>

                <Grid item>
                    <Button size="small"
                            variant="contained"
                            color="primary"
                            onClick={checkGuess}
                            value={guess}
                            onChange={handleChange}
                            className={"guess-input"}
                            disabled={guess !== '' || !hasStarted || !guess || hasLost}
                    >
                        Guess
                    </Button>
                </Grid>
                <Grid item>

                    <Button
                        size="small"
                        variant="outlined"
                        color="primary"
                        onClick={randomizeMinus}
                        disabled={!hasStarted || hasLost}
                    >
                        I don't Know
                    </Button>
                </Grid>
                <Grid item>
                    <Button
                        size="small"
                        variant="outlined"
                        color="primary"
                        disabled={!hasLost}
                        onClick={enableName}
                    >
                        New Game
                    </Button>


                </Grid>


                <Grid item>
                    <Chip
                        label={hasStarted ?
                            points :
                            "0"}
                        color="primary"

                    />
                </Grid>

                <Grid item>
                    <Chip className={classes.chip}
                          label=
                              {hasStarted ?
                                  <Timer time={timeLeft}/>
                                  :
                                  "0"}
                          color="secondary"

                    />
                </Grid>


            </Grid>


            <Grid container spacing={3}>

                <Grid item xs>

                    <TextField
                        disabled={hasLost}
                        label={
                            hasStarted ?
                                "What's That Song?" :
                                "What's Your Name?"
                        }
                        placeholder={
                            hasStarted ?
                                placeholderMsg :
                                placeholderMsgName}
                        fullWidth
                        type="text"
                        value={
                            hasStarted ?
                                guess :
                                playerName}
                        onChange={hasStarted ?
                            handleChange :
                            handleChangeName}
                        onKeyDown={keyPress}
                    />
                </Grid>

            </Grid>
            <Grid container spacing={3}>

                <Grid item xs className={classes.randomChorus}>


                    <Typography variant="h2">
                        {hasLost ?
                            gameOverMsg :
                            hasStarted ?
                                randomRefrain :
                                gameStartMsg}
                    </Typography>
                </Grid>
            </Grid>
        </div>
    );
}
