const RefrainsData =

    [
        {
            id: 1,
            refrain: "Und alles nur, weil ich dich liebe! Und ich nicht weiß, wie ich's beweisen soll!",
            song: "alles aus liebe",
            genre: "rock"
        },
        {
            id: 2,
            refrain: "I bless the rains down in Africa",
            song: "africa",
            genre: "classics"
        },
        {
            id: 3,
            refrain: "It's the Ace Of Spades, the Ace of Spades",
            song: "ace of spades",
            genre: "rock"
        },
        {
            id: 4,
            refrain: "Wo ist denn der Held, der mit seinem Geld die Runde bestellt?",
            song: "altbierlied",
            genre: "rock"
        },
        {
            id: 5,
            refrain: "Bermuda, Bahama, come on pretty mama",
            song: "kokomo",
            genre: "classics"
        },
        {
            id: 6,
            refrain: "If you like piña coladas\n" +
                "And getting caught in the rain",
            song: "escape",
            genre: "classics"
        },
        {
            id: 7,
            refrain: "I want to get away, I wanna fly away",
            song: "fly away",
            genre: "rock"
        },
        {
            id: 8,
            refrain: "You used to call me on my cell phone\n" +
                "Late-night when you need my love",
            song: "hotline bling",
            genre: "rap"
        },
        {
            id: 9,
            refrain: "Basically, I'm sayin', either way, we 'bout to slide, ayy\n" +
                "Can't let this one slide, ayy",
            song: "toosie slide",
            genre: "rap"
        },
        {
            id: 10,
            refrain: "Yung Lean in the club\n" +
                "For some morphine (Morphine)",
            song: "ginseng strip",
            genre: "rap"
        },
        {
            id: 11,
            refrain: "Shawty what you sippin' on? Gatorade\n"+
                "Shawty what you smokin' on? Lemonade",
            song: "gatorade",
            genre: "rap"
        },
        {
            id: 12,
            refrain: "Whoa, yeah\n" +
                "Kickstart my heart, hope it never stops\n" +
                "Ooh, yeah\n" +
                "Baby, yeah",
            song: "kickstart my heart",
            genre: "rock"
        },
        {
            id: 13,
            refrain:
                " No one compares to you\n" +
                "I'm scared that you\n" +
                "Won't be waiting on the other side",
            song: "dark paradise",
            genre: "pop"
        },
        {
            id: 14,
            refrain:
                "There’s something behind his eyes\n" +
                "That no one can read\n" +
                "Beyond their comprehension\n" +
                "And the words that he speaks",
            song: "the dream soul",
            genre: "metal"
        },
        {
            id: 15,
            refrain:
                "And if my insecurities won’t bore you\n" +
                "I’m sure my sex life will.",
            song: "love songs",
            genre: "rock"
        },
        {
            id: 16,
            refrain:
                "Dark days behind me\n" +
                "Hope the good days don't blind me",
            song: "dark days",
            genre: "country"
        },
        {
            id: 17,
            refrain:
                "If you’re travelin' in the north country fair\n" +
                "Where the winds hit heavy on the borderline",
            song: "girl from the north country",
            genre: "country"
        },
        {
            id: 18,
            refrain:
                "Do I stay inside 'til I see the sun?\n" +
                "Once again, let the seasons run?",
            song: "seasons run",
            genre: "pop"
        },
        {
            id: 19,
            refrain:
                "Can you help me occupy my brain?",
            song: "paranoid",
            genre: "metal"
        },
        {
            id: 20,
            refrain:
                "But here I am, on the road again\n" +
                "There I am, up on the stage",
            song: "turn the page",
            genre: "metal"
        },
        {
            id: 21,
            refrain:
                "Sweet, sweet heart of mine\n" +
                "I'm goin' to break again a million times",
            song: "stone",
            genre: "country"
        },
        {
            id: 22,
            refrain:
                "He was a Sk8er Boi\n" +
                "She said, \"See You Later, Boy\"",
            song: "sk8er boi",
            genre: "pop"
        },
        {
            id: 23,
            refrain:
                "For every day I'm workin' on the Illinois River\n" +
                "Get a half-a-day off with pay",
            song: "long hot summer day",
            genre: "country"
        },
        {
            id: 24,
            refrain:
                "Flash before my eyes\n" +
                "Now it's time to die\n" +
                "Burning in my brain\n" +
                "I can feel the flame",
            song: "ride the lightning",
            genre: "metal"
        },
        {
            id: 25,
            refrain:
                "We be all night, love, love\n" +
                "We be all night, love, love",
            song: "drunk in love",
            genre: "pop"
        },
        {
            id: 26,
            refrain:
                "Work Work Work Work Work",
            song: "work",
            genre: "pop"
        }
    ];


export default RefrainsData
