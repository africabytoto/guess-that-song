import React, {useState} from "react";
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from "@material-ui/core/Button";

function WelcomeComponent(props) {

    const handleChange = event => setName(event.target.value);

    const [name, setName] = useState("");
    const placeholderMsg = "Enter Your Name";

    const [hasEntered, setHasEntered] = useState(false);


    function checkName() {
        if (name)
            setHasEntered(prevState => !prevState)
        setName(prevState => "")
        console.log(name)

    }


    function keyPress(e) {
        if (e.keyCode === 13) {
            if (name) {

                checkName()
                window.alert("Hallo " + name);

            }
        }
    }

    return (
        <Grid item xs>
            <TextField
                label={"Enter Your Name and Hit Enter!"}
                placeholder={"Your Name"}
                type="text"
                fullWidth
                value={name}
                onChange={handleChange}
                onKeyDown={keyPress}
            />

            <Grid item>
                <Button
                    size="small"
                    variant="outlined"
                    color="primary"
                    onClick={checkName}
                >
                    Check Name
                </Button>


            </Grid>

            <Grid item>

            </Grid>
        </Grid>


    )


}

export default WelcomeComponent
