import React, {useEffect, useState} from "react";


function Timer(props) {


    const [timeLeft, setTimeLeft] = useState(props.time)


    useEffect(() => {
            const interval = setInterval(() => {
                setTimeLeft(prevTimeLeft => prevTimeLeft - 1);
            }, 1000);
            return () => {
                clearInterval(interval)
            };
        }, []
    );

    if (timeLeft <= 0) {
    return {timeLeft}
    }
    return {timeLeft}
}

export default Timer;
