Guessing Game built with ReactJS and Material-Ui (WIP)

The idea behind this game is simple: 
Enter your name and guess the chorus of a random song.
For every right guess you will get one point and for every miss, one point will be removed.
One round takes 10 seconds or until you have 0 points.

Guess That Song is still very much WIP and my personal project to gain proficiency in ReactJS and Material-Ui

HAVE FUN & Let me know your high score :)